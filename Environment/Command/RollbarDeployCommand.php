<?php

namespace Iweigel\DeployBundle\Environment\Command;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class RollbarDeployCommand extends AbstractCommand
{

    /**
     * @var array
     */
    protected $args = array();

    /**
     * @param array $args
     * @throws \RuntimeException
     * @return string
     */
    public function getCommand(array $args)
    {
        $args = $this->getArguments($args);
        if(!array_key_exists('token',$args)){
            throw new \RuntimeException("Need token argument");
        }
        if(!array_key_exists('environment',$args)){
            throw new \RuntimeException("Need environment argument");
        }

        $command = 'curl --request POST https://api.rollbar.com/api/1/deploy/ --data "access_token=' . $args['token'] . '&environment=' . $args['environment'] . '&revision=$(git log -n 1 --pretty=format:"%h")&comment=$(git log -n 1 --pretty=format:"%s")"';

        return $command;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rollbardeploy';
    }
}