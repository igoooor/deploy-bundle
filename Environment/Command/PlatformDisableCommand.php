<?php

namespace Iweigel\DeployBundle\Environment\Command;

use Symfony\Component\Console\Output\OutputInterface;

class PlatformDisableCommand extends AbstractSymfonyCommand
{
    /**
     * @param array $args
     * @return string
     */
    public function getCommand(array $args)
    {
        return 'iweigel:maintenance:disable-platform';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'disableplatform';
    }
}