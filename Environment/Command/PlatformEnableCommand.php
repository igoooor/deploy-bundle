<?php

namespace Iweigel\DeployBundle\Environment\Command;

use Symfony\Component\Console\Output\OutputInterface;

class PlatformEnableCommand extends AbstractSymfonyCommand
{
    /**
     * @param array $args
     * @return string
     */
    public function getCommand(array $args)
    {
        return 'iweigel:maintenance:enable-platform';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'enableplatform';
    }
}