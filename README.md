IweigelDeployBundle
==================

Configuration

```yaml
iweigel_deploy:
    server: %deploy_server%
    environment: %deploy_environment%
    basic_auth_users:
        - { user: iweigel, pass: 4EmbOAwVyiOSFFLI }
        - { user: projectname, pass: projectpass }
    immediate_process_strategies:
        runabove:
            serviceid: iweigel_deploy.server.immediateprocessstrategy.runabove
            options:
                user: apollonia
                passphrase: %deploy_runabove_ssh_passphrase%
                publicKeyFile: %deploy_runabove_ssh_publickeyfile%
                privateKeyFile: %deploy_runabove_ssh_publickeyfile%
                config: %deploy_runabove_ssh_config%
    server_environments:
        localhost_dev:
            cacheclear:
                - {priority: 1, args: { symfonyEnv: dev }}
            assetsinstall:
                - {priority: 2}
            asseticdump:
                - {priority: 3, args: { symfonyEnv: dev }}
            doctrineschemaupdate:
                - {priority: 4, args: { symfonyEnv: dev, force: true, complete: true, dumpSql: false }}
        runabove_dev:
            cacheclear:
                - {priority: 1, args: { symfonyEnv: dev }}
                - {priority: 2, args: { symfonyEnv: prod }}
            assetsinstall:
                - {priority: 3}
            asseticdump:
                - {priority: 4, args: { symfonyEnv: dev }}
                - {priority: 5, args: { symfonyEnv: prod }}
            doctrineschemaupdate:
                - {priority: 6, args: { symfonyEnv: dev, force: true, complete: true, dumpSql: false }}
        runabove_test:
            mysqldump:
                - {priority: 1, args: { path: ~/backup/database }}
            cacheclear:
                - {priority: 2, args: { symfonyEnv: dev }}
                - {priority: 3, args: { symfonyEnv: prod }}
            assetsinstall:
                - {priority: 4}
            asseticdump:
                - {priority: 5, args: { symfonyEnv: dev }}
                - {priority: 6, args: { symfonyEnv: prod }}
            doctrineschemaupdate:
                - {priority: 7, args: { symfonyEnv: dev, force: true, complete: true, dumpSql: false }}
        runabove_production:
            mysqldump:
                - {priority: 1, args: { path: ~/backup/database }}
            #opcachereset: # - not needed on runabove server because switch_env command on shell will also clear opcache
                #- {priority: 2, args: { host: 'integration.projectname.atri.iweigel.ch', user: iweigel, pass: 4EmbOAwVyiOSFFLI }}
            cacheclear:
                - {priority: 3, args: { symfonyEnv: dev }}
                - {priority: 4, args: { symfonyEnv: prod }}
            assetsinstall:
                - {priority: 5}
            asseticdump:
                - {priority: 6, args: { symfonyEnv: dev }}
                - {priority: 7, args: { symfonyEnv: prod }}
        runabove_*:
            writebasicauthusersfile:
                - {priority: 0}
```

Routing (for OpCache Reset) - not needed on runabove server because switch_env command on shell will also clear opcache

```yaml
# IweigelDeployBundle (for OpCache Reset)
iweigel_deploy:
    resource: "@IweigelDeployBundle/Controller/"
    type:     annotation
    prefix:   /
```

Security (for OpCache Reset) - not needed on runabove server because switch_env command on shell will also clear opcache

```yaml
security:
    access_control:
        - { path: ^/iweigel/deploy/opcache/reset, role: IS_AUTHENTICATED_ANONYMOUSLY }
```

parameters.yml.dist (DO NOT SET DEFAULT VALUES ON deploy_server OR deploy_environment!) If you set default server to 'localhost' and environment to 'dev' it's verly likely that a production server will get those parameters as well and start to schema update with --force --complete for example

```yaml
deploy_server: ~
deploy_environment: ~
deploy_runabove_ssh_passphrase: ASK
deploy_runabove_ssh_publickeyfile: ~
deploy_runabove_ssh_privatekeyfile: ~
deploy_runabove_ssh_config: ~
```

composer.json

```json
"require": {
    "iweigel/deploy-bundle": "2.*"
},
"scripts": {
    "post-install-cmd": [
        "Incenteev\\ParameterHandler\\ScriptHandler::buildParameters",
        "Sensio\\Bundle\\DistributionBundle\\Composer\\ScriptHandler::buildBootstrap",
        "Sensio\\Bundle\\DistributionBundle\\Composer\\ScriptHandler::installRequirementsFile",
        "Iweigel\\DeployBundle\\Composer\\ScriptHandler::deploy"
    ],
    "post-update-cmd": [
        "Incenteev\\ParameterHandler\\ScriptHandler::buildParameters",
        "Sensio\\Bundle\\DistributionBundle\\Composer\\ScriptHandler::buildBootstrap",
        "Sensio\\Bundle\\DistributionBundle\\Composer\\ScriptHandler::installRequirementsFile",
        "Iweigel\\DeployBundle\\Composer\\ScriptHandler::deploy"
    ]
},
"repositories": [
    {
        "type": "vcs",
        "url": "git@codebasehq.com:iweigel/iweigelch/iweigeldeploybundle.git"
    }
]
```

AppKernel

```php
new Iweigel\DeployBundle\IweigelDeployBundle()
```