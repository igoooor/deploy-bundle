<?php

namespace Iweigel\DeployBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class CompilerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $environmentManagerDefinition = $container->getDefinition('iweigel_deploy.environment.manager');
        foreach($container->findTaggedServiceIds('iweigel_deploy.command') as $serviceId => $tags){
            $environmentManagerDefinition->addMethodCall('addCommand', array(new Reference($serviceId)));
        }

        $environmentManagerDefinition = $container->getDefinition('iweigel_deploy.server.immediateprocessmanager');
        foreach($container->findTaggedServiceIds('iweigel_deploy.immediateprocessstrategy') as $serviceId => $tags){
            $environmentManagerDefinition->addMethodCall('addStrategy', array($serviceId, new Reference($serviceId)));
        }
    }
}